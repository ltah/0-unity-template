# Unity Template

A blank Unity 5 template with the files for [UnityVS](http://unityvs.com/) for integration with Visual Studio 2013. This may be expanded as other libraries are found or created.

## Requirements

* Unity 5
* Visual Studio 2013